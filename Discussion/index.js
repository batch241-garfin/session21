
// ARRAYS //

let studentNumberA = '2023-1923';
let studentNumberB = '2023-1924';
let studentNumberC = '2023-1925';
let studentNumberD = '2023-1926';
let studentNumberE = '2023-1927';

let studentNumbers = ['2023-1928', '2023-1929','2023-1930'];
console.log(studentNumbers);

let grades = [98.5,94.3,99.01,90.1];
console.log(grades);

let computerBrands = ["Acer","Asus","Lenovo","Neo","Redfox","Gateway"];
console.log(computerBrands);

let mixedArr = [12,'Asus',null,undefined, {}];
console.log(mixedArr);

let myTasks = ['drink HTML',"eat Javascript",
	'inhale CSS',"bake Sass"];
	console.log(myTasks);

// Creating an Array with values from variables //

	let city1 = "Tokyo";
	let city2 = "Manila";
	let city3 = "Jakarta";

	let cities = [city1,city2,city3];
	console.log(cities);

// Length Property //

	// .length property allows us to get and set the total number of
	// items in an array

console.log(myTasks.length); //4
console.log(cities.length); //3


let blankArr = [];
console.log(blankArr.length);

cities.length--;
console.log(cities);
cities.length--;
console.log(cities);

let theBeatles = ["John","Paul","Ringo","George"];
theBeatles.length++;
console.log(theBeatles);

// Reading from Arrays //

// In Javascript, the first element is associated with the number 0.
// Array indexes actually refer to an address/location in the
// device's memory and not the information is stored.

// Syntax:
// arrayName[index];

console.log(grades[0]); // 98.5
console.log(computerBrands[1]); // Asus

console.log(grades[20]); // undefined

let lakersLegend = ["Kobe","Shaq","Lebron","Magic","Kareem"];

console.log(lakersLegend[1]); // Shaq
console.log(lakersLegend[3]); // Magic

let currentlakersLegend = lakersLegend[2];
console.log(currentlakersLegend); // Lebron

//Accessing the last elements of an Array

let  bullsLegends = ["Jordan","Pipen","Rodman","Rose","Kukoc"];
let lastElementIndex = bullsLegends.length - 1;
console.log(bullsLegends[lastElementIndex]);
console.log(bullsLegends[bullsLegends.length-1]);


let newArr = [];
console.log(newArr[0]);

newArr[0] = "Cloud Strife";
console.log(newArr);

console.log(newArr[1]);
newArr[1] = "Tifa Lockhart";
console.log(newArr);

newArr[1] = "Jeru Palma";
console.log(newArr);

newArr[2] = "Mica Tingcang";
console.log(newArr);

newArr[newArr.length] = "Jenno Vea";
console.log(newArr);

// Looping over an Array

for (let index = 0; index < newArr.length; index++)
{
	console.log(newArr[index]);
}

let numArr = [5,12,30,46,40];

for(let index = 0; index < numArr.length; index++)
{
	if (numArr[index] % 5 === 0)
	{
		console.log(numArr[index] + " is divisible by 5");
	}
	else
	{
		console.log(numArr[index] + " is not divisible by 5");
	}
}

// Multidimensional Arrays

let chessboard = [['a','b','c','d','f','g','h'],[1,2,3,4,5,6,7,8]];